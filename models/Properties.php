<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "properties".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $image
 * @property string $description
 */
class Properties extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'properties';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name','image', 'description'], 'required'],
            [['user_id'], 'integer'],
            [['description'], 'string'],
            [['name','image'], 'string', 'max' => 255],
            //[['image'], 'safe'],
            [['file'], 'file']
            //[['image'], 'file', 'types' => 'png'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'image' => 'Image',
            'description' => 'Description',
        ];
    }
}
