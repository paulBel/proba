<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\sqlUser;


/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegisterForm extends Model
{
    public $username;
    public $password;
    public $rememberMe;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            //['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            //['password', 'validatePassword'],
        ];
    }



    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function register()
    {
        $user=new sqlUser();
        if(sqlUser::findByUsername($this->username)!=null){
            $this->addError( 'Username already exists.');
            return false;
        }else{
            $user->username=$this->username;
            $user->password=$this->password;
            $user->save(false);
            return true;
        }

    }





}
