<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                
                'name',
                [
                    'attribute' => 'image',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img( $data['image'],
                            ['width' => '70px']);
                    },
                ],
                'description:ntext',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'visibleButtons' => [
                        'delete' => false,
                        'update' => false
                    ],
                    'controller' => 'properties'
                ],
            ],
        ]); ?>

    </div>
</div>
