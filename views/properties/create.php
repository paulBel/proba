<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Properties */

$this->title = 'Create Properties';
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="properties-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    /*echo $this->render('_form', [
        'model' => $model,
    ]) */
    ?>


    <?php

    $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => [['class' => 'form-horizontal'],['enctype'=>'multipart/form-data']]
    ]) ?>
    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'description')->textInput() ?>
    <?= $form->field($model, 'file')->fileInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>

</div>
